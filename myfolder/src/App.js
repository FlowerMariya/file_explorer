import './App.css';
import FileExplorer from './components/FileExplorer';

const App = () => {
  return (
    <div className="App">
      <FileExplorer />
    </div>
  );
}

export default App;
