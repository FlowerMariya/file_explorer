import React from 'react'
import { Menu, Input, Modal, Button, Col } from 'antd';
import {
    DesktopOutlined,
    FileOutlined,
    DownloadOutlined,
    AudioOutlined,
    PictureOutlined,
    VideoCameraOutlined,
    PlusCircleOutlined,
} from "@ant-design/icons";

const menuFile = (props) => {
    const { SetModalVisibleCreate, isModalVisibleCreate, createFolder, nameFolder, onCancel, setNameFolder } = props;
    return (
        <div >
            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" >
                <div className="logo">
                    <Button
                        style={{ marginLeft: '20px' }}
                        icon={<PlusCircleOutlined />}
                        onClick={() => SetModalVisibleCreate(true)}
                    >
                        Create Folder
                    </Button>
                    <Col xs={{ span: 24 }} md={{ span: 8 }} lg={{ span: 6 }}>
                        <Modal
                            title="Folder Name"
                            visible={isModalVisibleCreate}
                            onOk={() => createFolder()}
                            onCancel={() => onCancel()}>
                            <Input
                                placeholder="Folder Name"
                                value={nameFolder}
                                onChange={(e) => setNameFolder(e.target.value)}
                            />
                        </Modal>
                    </Col>
                </div>
                <Menu.Item key="1" icon={<DesktopOutlined />}>
                    Desktop
                </Menu.Item>
                <Menu.Item key="2" icon={<FileOutlined />}>
                    Documents
                </Menu.Item>
                <Menu.Item key="3" icon={<DownloadOutlined />}>
                    Downloads
                </Menu.Item>
                <Menu.Item key="4" icon={<AudioOutlined />}>
                    Music
                </Menu.Item>
                <Menu.Item key="5" icon={<PictureOutlined />}>
                    Pictures
                </Menu.Item>
                <Menu.Item key="6" icon={<VideoCameraOutlined />}>
                    Videos
                </Menu.Item>
            </Menu>
        </div>
    )
}

export default menuFile
