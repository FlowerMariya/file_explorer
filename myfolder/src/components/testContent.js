import React from 'react'
import { Input, Button, Col, Row } from 'antd';
import "./style.scss";

const testContent = (props) => {
    const { setRenameFoLder, editFolder, id, setEditFoldername } = props
    return (
        <div>
            <Input.Group size="small">
                <Row >
                    <Col span={14}>
                        <Input
                            size="large"
                            placeholder="Edit Folder Name"
                            onChange={event => setRenameFoLder(event.target.value)}
                        />
                    </Col> &nbsp;&nbsp;
                    <Col span={4}>
                        <Button
                            size="medium"
                            type="primary"
                            shape="circle"
                            onClick={() =>
                                editFolder(id)} >
                            Save
                        </Button>
                    </Col>
                    <Button
                        style={{ color: 'black' }}
                        size="medium"
                        type="link"
                        shape="circle"
                        onClick={() => setEditFoldername(false)} >
                        Cancel
                    </Button>
                </Row>
            </Input.Group>
        </div>
    )
}

export default testContent
