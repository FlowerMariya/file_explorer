import React from 'react'
import { Layout, Row } from 'antd';
import {
    LeftOutlined,
    RightOutlined,
    AppstoreOutlined,
    VerticalAlignMiddleOutlined,
    PicLeftOutlined,
    ShareAltOutlined,
    TagOutlined,
    SearchOutlined
} from "@ant-design/icons";
import "./style.scss";

const { Footer, Header } = Layout;

const layoutFile = (props) => {
    const { folderNamesArray, deleteFolder, folderView } = props
    return (
        <div>
            <Layout className="site-layout">
                <Header className="headerClass" >
                    <LeftOutlined className="leftIcon" />
                    <RightOutlined className="rightIcon" />&nbsp;&nbsp;&nbsp; Desktop
                    <AppstoreOutlined className="appIcon" />&nbsp;
                    <VerticalAlignMiddleOutlined className= "verticalIcon" />
                    <PicLeftOutlined className="picleftIcon" />
                    <ShareAltOutlined className="shareIcon" />
                    <TagOutlined className="tagIcon" />
                    <SearchOutlined className="searchIcon" />
                </Header>
                <div className="site-layout-background" /* style={{ padding: '25', minHeight: '85vh' }} */>
                    {folderNamesArray.length === 0 ? 
                        <p style={{ alignContent: 'center', color: 'grey' }}>This Folder Is Empty</p>
                        : <> {deleteFolder.id ? null : <Row gutter={4} style={{ marginLeft: '70px', marginTop: '20px' }}>{folderView}</Row>} </>
                    }
                </div>
                <Footer className="footerIcon"></Footer>
            </Layout>
        </div>
    )
}

export default layoutFile
