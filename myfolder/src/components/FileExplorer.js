import React, { useState } from 'react'
import { Layout, Menu, Button, Popover, Col, message, Popconfirm, Dropdown } from 'antd';
import {
    MoreOutlined,
} from "@ant-design/icons";
import "./style.scss";
import feedIcon from "./file3.png";
import Draggable from 'react-draggable';
import nextId from "react-id-generator";
import MenuFile from './menuFile';
import LayoutFile from './layoutFile';
import TestContent from './testContent';

const { Sider } = Layout;

const FileExplorer = () => {

    const [isModalVisibleCreate, SetModalVisibleCreate] = useState(false);
    const [editFoldername, setEditFoldername] = useState(false)
    const [deletePopUP, setDeletePopUP] = useState(false);
    const [deleteFolder, setDeleteFolder] = useState([])
    const [nameFolder, setNameFolder] = useState([])
    const [renameFoLder, setRenameFoLder] = useState([])
    const [folderNamesArray, setArrayName] = useState([])

    const createFolder = () => {
        SetModalVisibleCreate(false)
        folderNamesArray.push({
            id: nextId(),
            name: nameFolder,
        });
        setNameFolder('')
    };

    const editFolder = (id) => {
        if (id.length === 0) {
            message.warning("No Changes");
            return;
        }
        var index_value;
        folderNamesArray.findIndex(function (folderNamesArray, index) {
            if (folderNamesArray.id === id)
                index_value = index;
        })
        folderNamesArray[index_value].name = renameFoLder
        setEditFoldername(false)
    }

    const onCancel = () => {
        SetModalVisibleCreate(false)
        setNameFolder('')
    }

    const deleteWithId = (id) => {
        var index_value;
        folderNamesArray.findIndex(function (folderNamesArray, index) {
            if (folderNamesArray.id === id)
                index_value = index;
        })
        setDeleteFolder(folderNamesArray.splice(index_value, 1))
    }

    const testContent = (id) => (
        <TestContent
            setRenameFoLder={setRenameFoLder}
            editFolder={editFolder}
            id={id}
            setEditFoldername={setEditFoldername}
        />
    );

    const menu = (id) => (
        <Menu>
            <Menu.Item>
                <Popover
                    content={testContent(id)}
                    visible={editFoldername}
                    title="FolderName"
                    trigger="click">
                    <Button onClick={() => setEditFoldername(true)}>Edit</Button>
                </Popover>
            </Menu.Item>
            <Menu.Item>
                <Popconfirm
                    title="Are you sure to delete this folder?"
                    onConfirm={() => deleteWithId(id)}
                    onCancel={() => setDeletePopUP(false)}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button onClick={() => setDeletePopUP(true)}>Delete</Button>
                </Popconfirm>
            </Menu.Item>
        </Menu>
    )

    let folderView = folderNamesArray.map((item) => {
        return (
            <Draggable>
                <Col xs={{ span: 24 }} md={{ span: 24 }} lg={{ span:4 }}>
                    <img width="80px" height="90px" src={feedIcon} />
                    <Dropdown placement="bottomLeft" overlay={menu(item.id)} trigger={['click']}>
                        <MoreOutlined style={{ color: 'white' }} />
                    </Dropdown>
                    <b><h3 style={{ color: 'white' }}>{item.name}</h3></b>
                </Col>
            </Draggable>
        );
    });

    return (
        <div className="screenSize"  >
            <Layout>
                <Sider
                    breakpoint="lg"
                    collapsedWidth="0"
                    onBreakpoint={broken => {
                        console.log(broken);
                    }}
                    onCollapse={(collapsed, type) => {
                        console.log(collapsed, type);
                    }} >
                    <MenuFile
                        SetModalVisibleCreate={SetModalVisibleCreate}
                        isModalVisibleCreate={isModalVisibleCreate}
                        createFolder={createFolder}
                        onCancel={onCancel}
                        nameFolder={nameFolder}
                        setNameFolder={setNameFolder}
                    />
                </Sider>
                <LayoutFile
                    folderNamesArray={folderNamesArray}
                    deleteFolder={deleteFolder}
                    folderView={folderView}
                />
            </Layout>
        </div >
    )
}

export default FileExplorer